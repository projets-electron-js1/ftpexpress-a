const {app, BrowserWindow, Menu, ipcMain, webContents} = require('electron')
const url = require("url");
const path = require("path");
const os = require("os")
const zipLib = require("zip-lib")
const { Client } = require("basic-ftp")

let mainWindow

// === Menu template ===
const menuTemplate = [
  {
    label: 'File',
    submenu: [
      {
        label: 'Quit',
        click: () => app.quit(),
        accelerator: 'Ctrl+W' 
      }
    ]
  }
] 

function createWindow () {
  // ipcMain.handle('appli:quit',app.quit()),
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: false,
      contextIsolation: true,
      preload: path.join(__dirname,'preload.js')
    }
  })
  
  // couleur d'arrière-plan
  mainWindow.setBackgroundColor('rgb(46,130,227)')

  mainWindow.loadURL(
    url.format({
      pathname: path.join(__dirname,`/dist/ftpxpress-a/index.html`),
      protocol: "file:",
      slashes: true
    })
  );
  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  mainWindow.on('closed', function () {
    mainWindow = null
  })

  // Implentation du menu
  const mainMenu = Menu.buildFromTemplate(menuTemplate)
  Menu.setApplicationMenu(mainMenu)

  // --- ANGULAR => appli:quit
  ipcMain.on('appli:quit',async (event) => {
    app.quit()
  })

  // --- ANGULAR => tmpPath:get
  ipcMain.handle('tmpPath:get',async (event) => {
    try {
      return os.tmpdir() 
    } catch(error) {
      return 'KO'
    }
  })

  //  --- ANGULAR => zip:create
  ipcMain.handle('zip:create',async (event,args) => {
    zipLib.archiveFile(args.inPath,args.outPath)
    return
  }),

  // --- ANGULAR => ftp:transfert
  ipcMain.handle('ftp:transfert',async (event,args) => {
    const ftpClient = new Client
    const uploadFile = args.zipPath.split("\\").pop()
    ftpClient.ftp.verbose = true
    await ftpClient.access({
      host: "ftp.dlptest.com",
      user: "dlpuser",
      password: "rNrKYTX9g7z3RgJRmxWuGHbeu",
      secure: false
    })
    await ftpClient.uploadFrom(args.zipPath, uploadFile)
    return
  })

}

app.on('ready',createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  if (mainWindow === null) createWindow()
})