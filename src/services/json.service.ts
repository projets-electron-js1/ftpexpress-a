import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class JsonService {

  constructor(private http: HttpClient) { }

  read(filePath: string): Observable<any> {
    return this.http.get<any>(filePath).pipe(
      catchError((error: HttpErrorResponse) => {
        console.error('Une erreur s\'est produite lors du chargement du fichier JSON :', error.message);
        return throwError('Erreur lors du chargement du fichier JSON');
      })
    );
  }
}