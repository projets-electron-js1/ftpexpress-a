const { contextBridge, ipcRenderer } = require('electron');

// window.ipcRenderer = ipcRenderer
// window.ipcMain = ipcRenderer

contextBridge.exposeInMainWorld(
  'electron',
  {
    quit: () => ipcRenderer.send('appli:quit'),

    getTempPath: async () => await ipcRenderer.invoke('tmpPath:get'),

    zipFile: async (...args) => await ipcRenderer.invoke('zip:create',...args),

    ftpFile: async (...args) => await ipcRenderer.invoke('ftp:transfert',...args),

    testIpc: async (message) => await ipcRenderer.invoke('test:message',message)
  }
);