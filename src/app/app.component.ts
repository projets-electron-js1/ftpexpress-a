import { AfterViewInit, Component, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { JsonService } from 'src/services/json.service';
import { MessageService } from 'primeng/api';

interface FileWithPath extends File {
  path: string
}

const electron = (window as any).electron

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [MessageService]
})
export class AppComponent implements AfterViewInit{
  
  // --- Déclarations des propriétés
  title = 'ftpxpress-a';
  parametersPath = './assets/json/parameters.json'
  tempPath =''
  parameters: any
  initParameters: boolean = false
  initTmpPath: boolean = false
  dropZoneComment: string ='... glisser / déposer ici le fichier à transférer ...'
  dropFileOver = false
  zipFilePath =''
  dropFilePath =''
  dropFileCompleteName =''
  dropFileName =''
  dropFileExtension =''
  dropZoneDraggable: string = "true"
  readyTransfert: boolean = false

  @ViewChild("dropZone") dropZone!: ElementRef

  ngAfterViewInit(): void {
    this.dragOver()
    this.dragLeave()
    this.dropFile()
  }

  constructor (private _jsonService: JsonService,
              private _notification: MessageService,
              private _renderer2: Renderer2
  ) {}

  ngOnInit(): void {
    //
    // --- recupération dossier Temporaire via Electron
    electron.getTempPath().then( 
      (value : any) => {
        this.tempPath = value
        this.initTmpPath = true
      })
    //
    // --- lecture fichier JSon, contenant l'identifiant du royaume
    this._jsonService.read(this.parametersPath).subscribe(
      data => {
        this.parameters = data;
        // this.messageShow('Parameters file read','success')
        if (this.parameters.royaume === '') {
          this.messageShow('Critical - Data kingdom undefined','error')
        }
        this.initParameters = true
        this.testInitOk()
      },
      error => {
        this.parameters ={}
        this.messageShow('Critical - Parameters file missing','error')
      }
    );
  }

  testInitOk() {
    if (this.initTmpPath && this.initParameters) {
      this.messageShow('Initialization OK','success')
    }
  }

  droppedFile:any

  dragOver() {
    this._renderer2.listen(
      this.dropZone.nativeElement, 'dragover', (event : DragEvent) => {
        event.preventDefault()
        event.stopPropagation()
        this.dropFileOver =true
      }
    )
  }

  dragLeave() {
    this._renderer2.listen(
      this.dropZone.nativeElement, 'dragleave', (event : DragEvent) => {
        event.preventDefault()
        event.stopPropagation()
        this.dropFileOver =false
      }
    )
  }

  dropFile() {
    this._renderer2.listen(
      this.dropZone.nativeElement, 'drop', (event : DragEvent) => {
        event.preventDefault()
        event.stopPropagation()
        this.dropFileOver =false     
        this.dropFilePath = (event.dataTransfer?.files[0] as FileWithPath).path
        this.dropFileCompleteName = this.dropFilePath.slice(this.dropFilePath.lastIndexOf('\\')+1)
        this.dropFileName = this.dropFileCompleteName.slice(0,this.dropFileCompleteName.lastIndexOf('.'))
        this.dropFileExtension = this.dropFileCompleteName.slice(this.dropFileCompleteName.lastIndexOf('.')+1)
        switch (this.dropFileExtension) {
          case 'zip':
            this.messageShow('Fichier prêt au transfert','success')
            this.dropZoneComment = this.dropFilePath + ' prêt au transfert'
            this.readyTransfert = true
            this.zipFilePath = this.dropFilePath
            break
          case 'xlsx':
          case 'xlsm':
            this.messageShow('Lancement zippage du fichier','info')
            this.dropZoneComment = 'Zippage de ' + this.dropFilePath + ' en cours ...'
            this.readyTransfert = false
            this.zipFilePath = this.tempPath + "\\" + this.dropFileName + '.zip'
            this.zipFile()
            break
          default :
            this.messageShow('Sélectionner un fichier Excel ou ZIP','warn')
            this.resetTransfert()
        }
      }
    )
  }

  messageShow(messageText: string,messageType: string) {
    let stickyStatus = false
    if (messageType === 'error') {
      stickyStatus = true
      this.dropZoneDraggable = "false"
    }
    this._notification.clear()
    this._notification.add({
      severity: messageType,
      summary: '',
      detail: messageText,
      sticky: stickyStatus,
      life: 5000,
      closable: false
    })
  }

  zipFile() {
    // console.log('<-- ' + this.dropFilePath)
    // console.log('--> '+ this.zipFilePath)
    electron.zipFile({inPath:this.dropFilePath, outPath:this.zipFilePath}).then(
      (value : any) => {
        this.messageShow('Fichier prêt au transfert','success')
        this.dropZoneComment = this.zipFilePath + ' prêt au transfert'
        this.readyTransfert = true
      },
      (error : any) => {
        this.messageShow('Problème pendant le zippage','warn')
        this.resetTransfert
      }
    )
  }

  clickTransfert() {
    this.messageShow('Transfert FTP en cours','info')
    electron.ftpFile({zipPath:this.zipFilePath, filename:this.dropFileName + '.zip', dataKingdom:this.parameters.royaume}).then(
      (value : any) => {
        this.messageShow('Fichier transféré via FTP','success')
        this.resetTransfert()
      },
      (error : any) => {
        this.messageShow('Problème pendant le transfert FTP','warn')
        this.resetTransfert()
      }
    )
  }

  clickQuit() {
    electron.quit()
  }

  resetTransfert() {
    this.dropZoneComment = '... glisser / déposer ici le fichier à transférer ...'
    this.readyTransfert = false
    this.dropFilePath = ''
    this.zipFilePath = ''
  }

}


